#ifndef BASE_HPP
#define BASE_HPP

class GameObject {

protected:
	float mX = 0;
	float mY = 0;
	float mWidth = 0;
	float mHeight = 0;

public:
	GameObject(float x, float y, int width, int height);
	float getX();
	float getY();
	void setX(float x);
	void setY(float y);
	void setPosition(float x, float y);
	virtual void render() = 0;
};

#endif