#include <cstdio>
#include "crosshair.hpp"

Crosshair::Crosshair(char *path, int width, int height) : GameObject(0, 0, width, height) {
	if (!(mBitmap = al_load_bitmap(path))) {
		printf("Cannot load Crosshair with path: %s\n", path);
	}
	else {
		mBitmapWidth = al_get_bitmap_width(mBitmap);
		mBitmapHeight = al_get_bitmap_height(mBitmap);
		mWidth = width;
		mHeight = height;
	}
}

Crosshair::~Crosshair() {
	al_destroy_bitmap(mBitmap);
}

void Crosshair::render() {
	al_draw_scaled_bitmap(mBitmap, 0, 0, mBitmapWidth, mBitmapHeight, mX, mY, mWidth, mHeight, 0);
}