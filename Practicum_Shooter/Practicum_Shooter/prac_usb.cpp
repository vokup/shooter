#include "prac_usb.hpp"

PracUsb::PracUsb() {
	int result = libusb_init(&mContext);
	if (result < 0) {
		printf("Erorr occur while initializing \"libusb\"\n");
	}
}

bool PracUsb::getPracUsbDevice(PracUsbDevice **pracDevices) {
	libusb_device **devices;
	libusb_get_device_list(mContext, &devices);

	int pracCount = 0;
	libusb_device *dev;
	int i = 0;
	uint8_t path[8];

	while ((dev = devices[i++]) != NULL) {
		libusb_device_descriptor desc;
		int r = libusb_get_device_descriptor(dev, &desc);
		if (r < 0) {
			printf("failed to get device descriptor\n");
			return -1;
		}

		if (desc.idVendor == 0x16C0 && desc.idProduct == 0x05DC) {
			*pracDevices = new PracUsbDevice(mContext, dev);
			libusb_free_device_list(devices, 1);
			return true;
		}
	}
	libusb_free_device_list(devices, 1);

	return false;
}

void PracUsb::printDevices() {

	libusb_device **devices;
	libusb_get_device_list(mContext, &devices);

	libusb_device *dev;
	int i = 0, j = 0;
	uint8_t path[8];

	while ((dev = devices[i++]) != NULL) {
		libusb_device_descriptor desc;
		int r = libusb_get_device_descriptor(dev, &desc);
		if (r < 0) {
			fprintf(stderr, "failed to get device descriptor");
			return;
		}

		printf("%04x:%04x (bus %d, device %d)",
			desc.idVendor, desc.idProduct,
			libusb_get_bus_number(dev), libusb_get_device_address(dev));

		r = libusb_get_port_numbers(dev, path, sizeof(path));
		if (r > 0) {
			printf(" path: %d", path[0]);
			for (j = 1; j < r; j++)
				printf(".%d", path[j]);
		}
		printf("\n");
	}

	libusb_free_device_list(devices, 1);
}

void PracUsb::dispose() {
	libusb_exit(mContext);
}

PracUsb::~PracUsb() {
	dispose();
}

