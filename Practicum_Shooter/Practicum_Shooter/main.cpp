#include <cstdio>

#include <allegro5\allegro.h>
#include <allegro5\allegro_image.h>
#include <allegro5\allegro_color.h>
#include <allegro5\allegro_primitives.h>
#include <allegro5\allegro_font.h>
#include <allegro5\allegro_ttf.h>

#include <string>

#include <vector>
#include <inttypes.h>

#include "calculator_2.hpp"
#include "prac_usb.hpp"
#include "crosshair.hpp"
#include "target.hpp"

struct GunData {
	double yaw;
	double pitch;
	double roll;
	bool isSidePressed;
	bool isFirePressed;
};

const int ROUND_TIME = 60;
double mEleapsed = 0.0;

const int STATE_CALIB_TARGET1 = 0;
const int STATE_CALIB_TARGET2 = 1;
const int STATE_CALIB_TARGET3 = 2;
const int STATE_CALIB_TARGET4 = 3;
const int STATE_CALIB_TARGET5 = 4;
const int STATE_CALIB_END = 5;
int state_calib = STATE_CALIB_TARGET1;

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 720;

const int AL_EVENT_DISPLAY_CLOSE = 42;

void calibrationScreen();
void playScreen();
void endScreen();

// FOR PLAY SCREEN
Calculator2 *mCalc;
Crosshair *mCrosshair;
Target *mTarget;
double mEndTime = 60;
int mCHx = 0, mCHy = 0;
int mTGx = 0, mTGy = 0;
int mHitCount = 0;
bool mIsNextTarget = true;
char mTimerBuffer[16];

PracUsbDevice *pracDevices;
ALLEGRO_FONT *mMainFont32;
ALLEGRO_FONT *mMainFont48;
std::vector<GameObject*> mVecRender;
unsigned char mGunBuffer[64];
GunData mGunData;
void(*renderFunc)();

double mCenterHorAngle, mCenterVerAngle;
double mTopAngle, mBottomAngle;
double mLeftAngle, mRightAngle;

bool isHit(int chx, int chy, int tgx, int tgy) {
	int rchx = chx - 48;
	int rchy = chy - 48;
	int rtgx = tgx - 64;
	int rtgy = tgy - 64;

	return ((rchx - rtgx) * (rchx - rtgx) + (rchy - rtgy) * (rchy - rtgy)) <= (64 * 64);
}

int random(int from, int to) {
	return from + (rand() % static_cast<int>(from - to + 1));
}

double getAngle(double curAngle) {
	if (curAngle < 0) {
		return 360 + curAngle;
	}
	else {
		return curAngle;
	}
}

bool parseData(unsigned char *buffer, GunData *data) {
	/*for (int i = 0; i < 26; i++) {
		printf("%c", buffer[i]);
	}
	printf(" ");*/

	if (buffer[0] != '+' && buffer[0] != '-') {
		return false;
	}

	double sign = 1;
	if (buffer[0] == '-') {
		sign = -1;
	}
	data->yaw = (((buffer[1] - '0') * 100) + ((buffer[2] - '0') * 10) + (buffer[3] - '0') + ((buffer[5] - '0') * 0.1) + ((buffer[6] - '0') * 0.01)) * sign;

	sign = 1;
	if (buffer[8] == '-') {
		sign = -1;
	}
	data->pitch = (((buffer[9] - '0') * 100) + ((buffer[10] - '0') * 10) + (buffer[11] - '0') + ((buffer[13] - '0') * 0.1) + ((buffer[14] - '0') * 0.01)) * sign;

	sign = 1;
	if (buffer[16] == '-') {
		sign = -1;
	}
	data->roll = (((buffer[17] - '0') * 100) + ((buffer[18] - '0') * 10) + (buffer[19] - '0') + ((buffer[21] - '0') * 0.1) + ((buffer[22] - '0') * 0.01)) * sign;

	data->isSidePressed = buffer[24] == 'T';
	data->isFirePressed = buffer[25] == 'T';

	return true;
}

void calibrationScreen() {
	Target target(128, 128);
	switch (state_calib) {
		case STATE_CALIB_TARGET1:
			Sleep(200);
			target.setPosition(SCREEN_WIDTH / 2 - 64, SCREEN_HEIGHT / 2 - 64);
			target.render();
			al_draw_text(mMainFont32, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 + 150, ALLEGRO_ALIGN_CENTER, "SHOOT 5 TARGETS TO CALIBRATION!");

			pracDevices->requestData(mGunBuffer);
			if (parseData(mGunBuffer, &mGunData)) {
				if (mGunData.isFirePressed) {
					Sleep(200);
					mCenterHorAngle = mGunData.yaw;
					mCenterVerAngle = mGunData.pitch;

					while (true) {
						pracDevices->requestData(mGunBuffer);
						if (parseData(mGunBuffer, &mGunData)) {
							if (mGunData.isFirePressed) {
								break;
							}
						}
					}
					state_calib = STATE_CALIB_TARGET2;
				}
			} 
			break;
		case STATE_CALIB_TARGET2:
			Sleep(200);
			target.setPosition(SCREEN_WIDTH / 2 - 64, 0);
			target.render();
			pracDevices->requestData(mGunBuffer);

			pracDevices->requestData(mGunBuffer);
			if (parseData(mGunBuffer, &mGunData)) {
				if (mGunData.isFirePressed) {
					Sleep(200);
					mTopAngle = mGunData.pitch;

					while (true) {
						pracDevices->requestData(mGunBuffer);
						if (parseData(mGunBuffer, &mGunData)) {
							if (!mGunData.isFirePressed) {
								break;
							}
						}
					}
					state_calib = STATE_CALIB_TARGET3;
				}
			}
			break;
		case STATE_CALIB_TARGET3:
			Sleep(200);
			target.setPosition(SCREEN_WIDTH / 2 - 64, SCREEN_HEIGHT - 128);
			target.render();

			pracDevices->requestData(mGunBuffer);
			if (parseData(mGunBuffer, &mGunData)) {
				if (mGunData.isFirePressed) {
					Sleep(200);
					mBottomAngle = mGunData.pitch;

					while (true) {
						pracDevices->requestData(mGunBuffer);
						if (parseData(mGunBuffer, &mGunData)) {
							if (!mGunData.isFirePressed) {
								break;
							}
						}
					}
					state_calib = STATE_CALIB_TARGET4;
				}
			}
			break;
		case STATE_CALIB_TARGET4:
			Sleep(200);
			target.setPosition(0, SCREEN_HEIGHT / 2 - 64);
			target.render();

			pracDevices->requestData(mGunBuffer);
			if (parseData(mGunBuffer, &mGunData)) {
				if (mGunData.isFirePressed) {
					Sleep(200);

					mLeftAngle = mGunData.yaw;
					while (true) {
						pracDevices->requestData(mGunBuffer);
						if (parseData(mGunBuffer, &mGunData)) {
							if (!mGunData.isFirePressed) {
								break;
							}
						}
					}
					state_calib = STATE_CALIB_TARGET5;
				}
			}
			break;
		case STATE_CALIB_TARGET5:
			Sleep(200);
			target.setPosition(SCREEN_WIDTH - 128, SCREEN_HEIGHT / 2 - 64);
			target.render();

			pracDevices->requestData(mGunBuffer);
			if (parseData(mGunBuffer, &mGunData)) {
				if (mGunData.isFirePressed) {
					Sleep(200);
					mRightAngle = mGunData.yaw;
					while (true) {
						pracDevices->requestData(mGunBuffer);
						if (parseData(mGunBuffer, &mGunData)) {
							if (!mGunData.isFirePressed) {
								break;
							}
						}
					}
					state_calib = STATE_CALIB_END;
				}
			}
			break;
		case STATE_CALIB_END:
			Sleep(200);
			al_draw_text(mMainFont48, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, ALLEGRO_ALIGN_CENTER, "PRESS FIRE BUTTON TO PLAY THE GAME!");
			//printf("%.2lf %.2lf %.2lf %.2lf %.2lf %.2lf\n", mCenterHorAngle, mCenterVerAngle, mTopAngle, mBottomAngle, mLeftAngle, mRightAngle);
			pracDevices->requestData(mGunBuffer);
			if (parseData(mGunBuffer, &mGunData)) {
				if (mGunData.isFirePressed) {
					Sleep(200);

					while (true) {
						pracDevices->requestData(mGunBuffer);
						if (parseData(mGunBuffer, &mGunData)) {
							if (!mGunData.isFirePressed) {
								break;
							}
						}
						
					}
					state_calib = STATE_CALIB_TARGET1;
					mEndTime = mEleapsed + ROUND_TIME;
					renderFunc = playScreen;
					mIsNextTarget = true;
					mHitCount = 0;
					//mCalc = new Calculator2(1.5, 1.4, 1.05, mCenterHorAngle, mCenterVerAngle, SCREEN_WIDTH, SCREEN_HEIGHT);
					mCalc = new Calculator2(0.75, 0.35, 0.20, mCenterHorAngle, mCenterVerAngle, SCREEN_WIDTH, SCREEN_HEIGHT);
				}
			}
			break;
	}
}

void playScreen() {
	if (mEndTime - mEleapsed <= 0) {
		renderFunc = endScreen;
	}

	if (mIsNextTarget) {
		mTGx = random(0, SCREEN_WIDTH - 128);
		mTGy = random(0, SCREEN_HEIGHT - 128);
		mTarget->setPosition(mTGx, mTGy);
		mIsNextTarget = false;
	}

	pracDevices->requestData(mGunBuffer);
	if (parseData(mGunBuffer, &mGunData)) {

		//printf("%d %d\n", mCalc->getX(mGunData.yaw), mCalc->getY(mGunData.pitch));
		mCHx = mCalc->getX(mGunData.yaw);
		mCHy = mCalc->getY(mGunData.pitch);
		mCrosshair->setPosition(mCHx, mCHy);

		if (mGunData.isSidePressed) {
			if (mGunData.isFirePressed) {
				if (isHit(mCHx, mCHy, mTGx, mTGy)) {
					mHitCount++;
					mIsNextTarget = true;
					printf("HIT\n");
				}

				while (true) {
					pracDevices->requestData(mGunBuffer);
					if (parseData(mGunBuffer, &mGunData)) {
						if (!mGunData.isFirePressed) {
							break;
						}
					}
				}
			}
		}
	}

	mTarget->render();
	mCrosshair->render();

	sprintf_s(mTimerBuffer, "TIME: %.0lf", mEndTime - mEleapsed);
	al_draw_text(mMainFont32, al_map_rgb(0, 0, 0), SCREEN_WIDTH - 80, 0, ALLEGRO_ALIGN_CENTER, mTimerBuffer);
}

void endScreen() {
	// Show Score and Press Fire button to go back to calibration Screen.
	std::string x = "YOU HIT " + std::to_string(mHitCount) + " TARGET(S)";
	al_draw_text(mMainFont48, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 - 48, ALLEGRO_ALIGN_CENTER, x.c_str());
	al_draw_text(mMainFont32, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 + 32, ALLEGRO_ALIGN_CENTER, "PRESS FIRE BUTTON TO PLAY AGAIN!");

	pracDevices->requestData(mGunBuffer);
	if (parseData(mGunBuffer, &mGunData)) {
		if (mGunData.isFirePressed) {
			while (true) {
				pracDevices->requestData(mGunBuffer);
				if (parseData(mGunBuffer, &mGunData)) {
					if (!mGunData.isFirePressed) {
						break;
					}
				}
			}
			renderFunc = calibrationScreen;
		}
	}
}

int main(int argn, const char **argv) {
	srand(time(0));

	PracUsb pracUsb;
	if (!pracUsb.getPracUsbDevice(&pracDevices)) {
		printf("Cannot find Practicum Board.\n");
		return EXIT_FAILURE;
	}

	if (!al_init()) {
		printf("Cannot initialize Allegro5.\n");
		return EXIT_FAILURE;
	}

	if (!al_init_image_addon()) {
		printf("Cannot initialize image addon.\n");
		return EXIT_FAILURE;
	}

	if (!al_install_keyboard()) {
		printf("Cannot install keyboard.\n");
		return EXIT_FAILURE;
	}

	if (!al_init_font_addon()) {
		printf("Cannot initialize font addon.\n");
		return EXIT_FAILURE;
	}

	if (!al_init_ttf_addon()) {
		printf("Cannot initialize ttf addon.\n");
		return EXIT_FAILURE;
	}

	bool is_running = true;
	al_set_new_display_flags(ALLEGRO_FULLSCREEN);
	ALLEGRO_DISPLAY *display = al_create_display(SCREEN_WIDTH, SCREEN_HEIGHT);
	ALLEGRO_EVENT_QUEUE *event_queue = al_create_event_queue();
	ALLEGRO_TIMER *render_timer = al_create_timer(1 / 60.0);

	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(render_timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());

	al_start_timer(render_timer);

	mMainFont32 = al_load_font("tahoma.ttf", 32, 0);
	mMainFont48 = al_load_font("tahoma.ttf", 48, 0);

	mTarget = new Target(128, 128);
	mCrosshair = new Crosshair("crosshair_red.png", 96, 96);
	renderFunc = calibrationScreen;

	ALLEGRO_EVENT cur_event;
	while (is_running) {
		al_wait_for_event(event_queue, &cur_event);

		switch (cur_event.type) {
			case AL_EVENT_DISPLAY_CLOSE:
				is_running = false;
				break;
			case ALLEGRO_EVENT_KEY_DOWN:
				if (cur_event.keyboard.keycode == 59) { // ESC
					is_running = false;
				}
				break;
			case ALLEGRO_EVENT_TIMER:
				mEleapsed = cur_event.timer.timestamp;
				//printf("%lf\n", cur_event.timer.timestamp);
				break;
			default:
				break;
		}

		al_clear_to_color(al_map_rgb(255, 255, 255));

		renderFunc();
		
		al_flip_display();
	}

	al_destroy_timer(render_timer);
	al_destroy_event_queue(event_queue);
	al_destroy_display(display);

	return EXIT_SUCCESS;
}