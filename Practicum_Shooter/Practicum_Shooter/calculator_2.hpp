#ifndef CALCULATOR_2_HPP
#define CALCULATOR_2_HPP

#include <cmath>

#define M_PI   3.14159265358979323846264338327950288

class Calculator2 {

	double mCenterHorAngle;
	double mCenterVerAngle;
	double mR;
	double mW;
	double mH;
	double mScreenWidth;
	double mScreenHeight;
	
public:
	Calculator2(double R, double W, double H, double centerHorAngle, double centerVerAngle, double screenWidth, double screenHeight);

	int getX(double curAngleInDegree);
	int getY(double curAngleInDegree);
};

#endif