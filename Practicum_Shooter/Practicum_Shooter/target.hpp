#ifndef TARGET_HPP
#define TARGET_HPP

#include <allegro5\allegro.h>
#include <allegro5\allegro_image.h>

#include "base.hpp"

class Target : public GameObject {
	/* BITMAP PROPERTIES */
	ALLEGRO_BITMAP *mBitmap = NULL;
	int mBitmapWidth = 0;
	int mBitmapHeight = 0;

public:
	Target(int width, int height);
	~Target();
	void render();
};

#endif