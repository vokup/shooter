#include "calculator_2.hpp"

Calculator2::Calculator2(double R, double W, double H, double centerHorAngle, double centerVerAngle, double screenWidth, double screenHeight) {
	mR = R;
	mW = W;
	mH = H;
	mCenterHorAngle = centerHorAngle;
	mCenterVerAngle = centerVerAngle;
	mScreenWidth = screenWidth;
	mScreenHeight = screenHeight;
}

int Calculator2::getX(double curAngleInDegree) {
	return (mScreenWidth / 2) - ((mR * tan((curAngleInDegree - mCenterHorAngle) * M_PI / 180.0)) / (mW / 2)) * (mScreenWidth / 2);
}

int Calculator2::getY(double curAngleInDegree) {
	return (mScreenHeight / 2) - ((mR * tan((curAngleInDegree - mCenterVerAngle) * M_PI / 180.0)) / (mH / 2)) * (mScreenHeight / 2);
}