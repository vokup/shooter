#ifndef PRAC_USB_DEVICE
#define PRAC_USB_DEVICE

#include <libusb.h>
#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <cstdlib>

class PracUsbDevice {
	const int BUFFER_SIZE = 64;

	libusb_context *mContext;
	libusb_device *mDevice;
	libusb_device_handle *mDeviceHandle;
	unsigned char *mMainBuffer;
	unsigned char *mTempBuffer;

	std::condition_variable mCvTransferingData;
	std::atomic<bool> mIsTransferingData;
	std::mutex mTransferingDataMutex;

	std::condition_variable mCvGetData;
	std::atomic<bool> mIsGettingData;
	std::atomic<bool>  mIsRunning;
	std::thread mUsbEventHandlerThread;

	void usbEventHandler(libusb_context *ctx);
	void swapBuffer();

public:
	const int GUN_DATA_SIZE = 26;

	PracUsbDevice(libusb_context *context, libusb_device *device);
	~PracUsbDevice();
	void getData(unsigned char *buffer);
	void requestData(unsigned char *buffer);
	void dispose();
};

#endif