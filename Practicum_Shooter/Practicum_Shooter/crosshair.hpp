#ifndef CROSSHAIR_HPP
#define CROSSHAIR_HPP

#include <allegro5\allegro.h>
#include <allegro5\allegro_image.h>

#include "base.hpp"

class Crosshair : public GameObject {
	/* BITMAP PROPERTIES */
	ALLEGRO_BITMAP *mBitmap = NULL;
	int mBitmapWidth = 0;
	int mBitmapHeight = 0;

public:
	Crosshair(char *path, int width, int height);
	~Crosshair();
	void render();
};

#endif