#ifndef PRAC_USB
#define PRAC_USB

#include <cstdlib>
#include <cstdio>
#include <libusb.h>

#include "prac_usb_device.hpp"

class PracUsb {
	libusb_context *mContext;

public:
	PracUsb();
	~PracUsb();

	bool getPracUsbDevice(PracUsbDevice **devices);
	void printDevices();
	void dispose();

	/**
	 * Allocation - libusb_alloc_transfer()
	 * Filling - libusb_fill_control_transfer(), libusb_fill_bulk_transfer(), libusb_fill_interrupt_transfer()
	 * Submission - libusb_submit_transfer()
	 * Deallocation - libusb_free_transfer()
	 * Cancellation - libusb_cancel_transfer()
	 * Resubmission
	 * Completion handling
	 */
};

#endif // !PRAC_USB
