#include "calculator.hpp"
#include <cstdio>

Calculator::Calculator(double R, double W, double H, double CD, double K, double horizontalStartAngle, double verticalStartAngle, double screenWidth, double screenHeight) {
	mR = R;
	mW = W;
	mH = H;
	mCD = CD;
	mK = K;
	mHorizontalStartAngle = horizontalStartAngle;
	mVerticalStartAngle = verticalStartAngle;
	mScreenWidth = screenWidth;
	mScreenHeight = screenHeight;

	mOffsetX = ((mW / 2.0f) - mCD) / W * mScreenWidth;
	mOffsetY = (K / H) * mScreenHeight;

	printf("%lf %lf\n", mOffsetX, mOffsetY);
}

double Calculator::getX(double curAngleInDegree) {
	return mOffsetX + 
		(mR * tan((curAngleInDegree - mHorizontalStartAngle) * M_PI / 180.0f)) * (mScreenWidth / mW);
}

double Calculator::getY(double curAngleInDegree) {
	return mOffsetY 
		+ (mR * tan((curAngleInDegree - mVerticalStartAngle) * M_PI / 180.0f)) * (mScreenHeight / mH);
}

double Calculator::getMinimumHorizontalAngle() {
	return mHorizontalStartAngle - atan(((mW / 2.0) - mCD) / mR) * 180.0 / M_PI;
}

double Calculator::getMaximumHorizontalAngle() {
	return mHorizontalStartAngle + atan(((mW / 2.0) + mCD) / mR) * 180.0 / M_PI;
}

double Calculator::getMinimumVerticalAngle() {
	return mVerticalStartAngle - atan(mK / mR) * 180.0 / M_PI;
}

double Calculator::getMaximumVerticalAngle() {
	return mVerticalStartAngle + atan((mH - mK) / mR) * 180.0 / M_PI;
}
