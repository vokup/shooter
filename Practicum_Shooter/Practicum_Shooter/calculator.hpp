#ifndef CALCULATOR
#define CALCULATOR

#include <cmath>

#define M_PI   3.14159265358979323846264338327950288

class Calculator {
	double mR;
	double mRX;
	double mRY;
	double mW;
	double mH;
	double mCD;
	double mK;
	double mScreenWidth;
	double mScreenHeight;
	double mHorizontalStartAngle;
	double mVerticalStartAngle;

	double mOffsetX;
	double mOffsetY;

public:
	Calculator(double R, double W, double H, double CD, double K, double horizontalStartAngle, double verticalStartAngle, double screenWidth, double screenHeight);

	double getX(double curAngleInDegree);
	double getY(double curAngleInDegree);
	double getMinimumHorizontalAngle();
	double getMaximumHorizontalAngle();
	double getMinimumVerticalAngle();
	double getMaximumVerticalAngle();
};

#endif