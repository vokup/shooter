#include <cstdio>
#include "target.hpp"

Target::Target(int width, int height) : GameObject(0, 0, width, height) {
	if (!(mBitmap = al_load_bitmap("target.png"))) {
		printf("Cannot load Target with path: target.png\n");
	}
	else {
		mBitmapWidth = al_get_bitmap_width(mBitmap);
		mBitmapHeight = al_get_bitmap_height(mBitmap);
		mWidth = width;
		mHeight = height;
	}
}

Target::~Target() {
	al_destroy_bitmap(mBitmap);
}

void Target::render() {
	al_draw_scaled_bitmap(mBitmap, 0, 0, mBitmapWidth, mBitmapHeight, mX, mY, mWidth, mHeight, 0);
}