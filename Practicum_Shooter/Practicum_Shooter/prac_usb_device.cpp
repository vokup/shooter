#include "prac_usb_device.hpp"

PracUsbDevice::PracUsbDevice(libusb_context *context, libusb_device *device) {
	mDevice = device;
	mIsRunning = true;
	mIsGettingData = false;
	mIsTransferingData = true;

	if (libusb_open(mDevice, &mDeviceHandle) != LIBUSB_SUCCESS) {
		printf("CANNOT CONNECT PRACTICUM BOARD");
		return;
	}

	if (libusb_claim_interface(mDeviceHandle, 0) &&
		libusb_kernel_driver_active(mDeviceHandle, 0)) {
		if (libusb_detach_kernel_driver(mDeviceHandle, 0) != LIBUSB_SUCCESS) {
			printf("UNABLE TO DETACH KERNEL DRIVER\n");
			return;
		}
	}

	mMainBuffer = new unsigned char[BUFFER_SIZE];
	mTempBuffer = new unsigned char[BUFFER_SIZE];
	//std::thread t(std::bind(&PracUsbDevice::usbEventHandler, this, mContext));
	//std::swap(mUsbEventHandlerThread, t);
}

void PracUsbDevice::swapBuffer() {
	unsigned char *temp = mMainBuffer;
	mMainBuffer = mTempBuffer;
	mTempBuffer = temp;
}

void PracUsbDevice::usbEventHandler(libusb_context *ctx) {
	std::mutex getDataMutex;
	while (mIsRunning) {

		int result = libusb_control_transfer(mDeviceHandle,
			LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_IN,
			123,
			0,
			0,
			mTempBuffer,
			26, 500);
	
		if (result == LIBUSB_ERROR_NO_DEVICE || result == LIBUSB_ERROR_OTHER) {
			printf("[PracUsbDevice::usbEventHandler] LIBUSB_ERROR_NO_DEVICE || result == LIBUSB_ERROR_OTHER\n");
			dispose();
			break;
		}
		
		if (result == 26) {

			std::unique_lock<std::mutex> lock(getDataMutex);
			while (mIsGettingData) {
				mCvGetData.wait(lock);
			};

			swapBuffer();
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(5));
	}
}

void PracUsbDevice::requestData(unsigned char *buffer) {
	int result = libusb_control_transfer(mDeviceHandle,
		LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_IN,
		123,
		0,
		0,
		buffer,
		26, 500);

	if (result == LIBUSB_ERROR_NO_DEVICE || result == LIBUSB_ERROR_OTHER) {
		printf("[PracUsbDevice::usbEventHandler] LIBUSB_ERROR_NO_DEVICE || result == LIBUSB_ERROR_OTHER\n");
		dispose();
	}
}

void PracUsbDevice::getData(unsigned char *buffer) {
	//printf("ENTER GET DATA\n");
	mIsGettingData = true;
	memcpy(buffer, mMainBuffer, GUN_DATA_SIZE);
	mIsGettingData = false;
	mCvGetData.notify_one();
}

void PracUsbDevice::dispose() {
	mIsRunning = false;
	delete[] mMainBuffer;
	delete[] mTempBuffer;

	libusb_close(mDeviceHandle);
}

PracUsbDevice::~PracUsbDevice() {
	dispose();
}
