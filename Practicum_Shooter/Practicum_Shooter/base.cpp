#include "base.hpp"

GameObject::GameObject(float x, float y, int width, int height) {
	mX = x;
	mY = y;
	mWidth = width;
	mHeight = height;
}

float GameObject::getX() {
	return mX;
}

float GameObject::getY() {
	return mY;
}

void GameObject::setX(float x) {
	mX = x;
}

void GameObject::setY(float y) {
	mY = y;
}

void GameObject::setPosition(float x, float y) {
	mX = x;
	mY = y;
}