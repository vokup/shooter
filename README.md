# Shooter
Shooter เป็นเกมที่ผู้เล่นจะต้องใช้ปืนยิงเป้าที่ปรากฏบนหน้าจอให้โดนให้มากที่สุดภายในเวลาที่กำหนด

โดยการพัฒนาจะใช้ Gyro และ Accelerometer sensor ที่กระบอกปืนซึ่งจะสื่อสารกับ IC Atmega8 ที่ทำหน้าที่

เป็นตัวกลางในการติดต่อกับ Computer ผ่าน USB โดยใช้การอ่านข้อมูลจาก Sensor ที่อัตรา 100 Hz เพื่อใช้ในการวัดและคำนวณเป้าบนหน้าจอ

สร้างขึ้นเพื่อเป็น Project ในวิชา Practicum for Computer Engineering ภายในภาควิชาวิศวกรรมคอมพิวเตอร์ มหาวิทยาลัยเกษตรศาสตร์

![Screenshot of Shooter](https://ecourse.cpe.ku.ac.th/tpm_media/12/photos/0014-50.jpg)
![Screenshot of Shooter](https://ecourse.cpe.ku.ac.th/tpm_media/12/photos/0014-20.jpg)

## ตัวอย่างการเล่นเกม
[![](http://img.youtube.com/vi/68hfj8RQvL8/0.jpg)](https://youtu.be/68hfj8RQvL8?t=102 "")

## Game Details
- **Creator:** [Thanapawee Phrachan](https://gitlab.com/vokup)
- **Delevopment Time:** 1 Month

### Computer Details
- **Language:** C++
- **Game Library:** Allegro
- **USB Library:** LibUSB

### Atmega8
- **Language:** C
- **Firmware:** USB Device Firmware
- **Data Polling Rate:** 100 Hz
- **Data Rate:** 2.53 KBytes

