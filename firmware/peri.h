#include <avr/io.h>
#include <util/delay.h>

#define IS_SWITCH_PRESSED()     (((PINC >> PC3) & 0x1) == 0)

void init_peripheral();
void set_led(uint8_t pin, uint8_t state);
void set_led_value(uint8_t value);
uint16_t read_adc(uint8_t channel);
uint16_t get_light();