#include "peri.h"

void init_peripheral() {
    /*DDRC = (DDRC | PIN(0) | PIN(1) | PIN(2)) & 0b11100111;
    PORTC = (PORTC | PIN(3)) & 0b11111000;*/
    DDRC = (DDRC | 0b00000111) & 0b11100111;
    PORTC = (PORTC | 0b00001000) & 0b11101000;
    _delay_ms(200);
    //DELAY(200);
}

void set_led(uint8_t pin, uint8_t state) {
    if (state) {
        PORTC = PORTC | (1 << pin);
    } else {
        PORTC = PORTC & (~(1 << pin));
    }
}

void set_led_value(uint8_t value) {
    PORTC = (PORTC & ~(0x7)) | (value & 0x7);
}

uint16_t read_adc(uint8_t channel) {
    ADMUX = (0 << REFS1) | (1 << REFS0) | (0 << ADLAR) | (channel & 0b1111);
    ADCSRA = (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0) | (1 << ADSC);
    while ((ADCSRA & (1 << ADSC)));
    return ADCL + ADCH * 256;
}

uint16_t get_light() {
    return read_adc(PC4);
}