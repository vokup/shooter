#define BAUD                115200

#include <avr/io.h>
#include <avr/interrupt.h>  /* for sei() */
#include <util/delay.h>     /* for _delay_ms() */
#include <util/setbaud.h>
#include <avr/pgmspace.h>   /* required by usbdrv.h */

#include "usbdrv.h"

#define RQ_GYRO_DATA                123
#define DATA_GYRO_READY             0
#define DATA_GYRO_NOT_READY         1
#define DATA_GYRO_LENGTH            24
#define DATA_REPORTED_LENGTH        26

uint8_t m_buffer[64];
uint8_t m_count = 0;
uint8_t m_is_begin_flag_found = 0;
uint8_t m_is_data_ready = DATA_GYRO_NOT_READY;

#define HEADER_LENGTH               5
const char HEADER[HEADER_LENGTH] = { '#', 'Y', 'P', 'R', '=' };
uint8_t m_header_index = 0;

void uart_init() {
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;

    #if USE_2X
    UCSR0A |= _BV(U2X0);
    #else
    UCSR0A &= ~(_BV(U2X0));
    #endif

    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); /* 8-bit data */
    UCSR0B = _BV(RXEN0) | _BV(TXEN0); /* Enable RX and TX */
}

void uart_write(char ch) {
    UDR0 = ch;
    loop_until_bit_is_set(UCSR0A, TXC0);
}

char uart_read() {
    loop_until_bit_is_set(UCSR0A, RXC0);
    return UDR0;
}

uint8_t is_fire_button_pressed() {
    return (PINC >> 2) & 0b1;
}

uint8_t is_side_button_pressed() {
    return (PINC >> 4) & 0b1;
}

/* ------------------------------------------------------------------------- */
/* ----------------------------- USB interface ----------------------------- */
/* ------------------------------------------------------------------------- */
usbMsgLen_t usbFunctionSetup(uint8_t data[8])
{
    usbRequest_t *rq = (void *)data;

    if (rq->bRequest == RQ_GYRO_DATA)
    {
        if (m_is_data_ready == DATA_GYRO_READY) {
            usbMsgPtr = m_buffer;
            m_is_data_ready = DATA_GYRO_NOT_READY;
            return DATA_REPORTED_LENGTH;
        } else {
            return 0;
        }
    }
    /* default for not implemented requests: return no data back to host */
    return 0;
}

void setup_gun() {
    DDRC = DDRC | 0b00010100;
    PORTC = PORTC | 0b00010100;

    _delay_ms(1000);
    uart_write(0xA5);
    uart_write(0x53);
}

void setup_board() {
    DDRD = DDRD | 0b00001000;
    PORTD = PORTD | 0b00001000;
}

/* ------------------------------------------------------------------------- */
int main(void)
{
    setup_board();

    usbInit();

    /* enforce re-enumeration, do this while interrupts are disabled! */
    usbDeviceDisconnect();
    _delay_ms(300);
    usbDeviceConnect();

    /* enable global interrupts */
    sei();

    uart_init();
    setup_gun();

    PORTD = PORTD & 0b11110111;

    /* main event loop */
    for(;;)
    {
        m_buffer[DATA_REPORTED_LENGTH - 2] = is_fire_button_pressed() ? 'F' : 'T';
        m_buffer[DATA_REPORTED_LENGTH - 1] = is_side_button_pressed() ? 'F' : 'T';

        if (m_is_data_ready == DATA_GYRO_NOT_READY) {
            uint8_t value = uart_read();
            if (m_is_begin_flag_found == 0 && value == HEADER[m_header_index]) {
                m_header_index++;

                if (m_header_index >= HEADER_LENGTH) {
                    m_is_begin_flag_found = 1;
                    m_header_index = 0;
                    m_count = 0;
                }
            } else if (m_is_begin_flag_found == 1) {
                m_buffer[m_count] = value;
                m_count++;

                if (m_count >= DATA_GYRO_LENGTH) {
                    m_is_data_ready = DATA_GYRO_READY;
                    m_is_begin_flag_found = 0;
                }
            }
        }

        usbPoll();
    }

    return 0;
}

/* ------------------------------------------------------------------------- */
